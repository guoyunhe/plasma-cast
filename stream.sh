ffmpeg \
   -video_size 1920x1080 -framerate 60 \
  -f x11grab -i :0.0+100,200 \
  -f alsa -i default \
  -f webm -cluster_size_limit 2M -cluster_time_limit 5100 -content_type video/webm \
  -c:a libvorbis -b:a 96K \
  -c:v libvpx -b:v 1.5M -crf 30 -g 150 -deadline good -threads 4 \
  icecast://source:hackme@localhost:8754/stream.webm

# http://localhost:8754/stream.webm

