# plasma-cast

Live stream your Plasma desktop to Chromecast and other devices

## Requirements

- ffmpeg: provide stream source
- icecast: web server to stream video and audio

