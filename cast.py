import time
import pychromecast

chromecasts = pychromecast.get_chromecasts()
print([cc.device.friendly_name for cc in chromecasts])

cast = chromecasts[0]
# Wait for cast device to be ready
cast.wait()
print(cast.device)

print(cast.status)

mc = cast.media_controller
mc.play_media('http://192.168.43.102:8754/stream.webm', 'video/webm')
mc.block_until_active()
print(mc.status)

mc.pause()
time.sleep(10)
mc.play()
